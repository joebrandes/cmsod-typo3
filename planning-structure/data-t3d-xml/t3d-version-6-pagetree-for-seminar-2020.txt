(TYPO3 Logo) [id=0] TYPO3 oberste Struktur  

-> Start [id=1] (Weltkugel Startseite/Rootpage - Verweis von/auf Unterpunkt Start, s. Menüs!)

	-> Start		   (Inhalt: Startseite)
	
	-> Module		   (Inhalt: Übersicht über Zertifikat)
		-> Übersicht          (Inhalt: Übersichtseite)
		-> Modul I            (Inhalt: Statische Websites)
		-> Modul II	          (Inhalt: Dynamische Websites)
		-> Modul IIIa         (Inhalt: Joomla)
		-> Modul IIIb         (Inhalt: TYPO3)
		-> Modul IIIc         (Inhalt: WordPress)
		
	-> Portalinfo	   (Inhalt: Kurzdarstellung Portalinfo)
		-> TYPO3 Core	      (Inhalt: Infos zum TYPO3 Core)
		-> TYPO3 Extensions   (Inhalt: Infos zu TYPO3 Extensions)
		-> Technik            (Inhalt: Technik des Portals)
		
	-> Testeiten 	   (Inhalt: Seitenbaum für Tests)
		-> Überblick	      (Inhalt: Überblick)
		-> Testseite A        (Inhalt: Testseite A)
		-> Testseite B        (Inhalt: Testseite B)
		-> Testseite C        (Inhalt: Testseite C)
		
	-> Spezialmenü 	   (Typ: Ordner - ID wichtig für das Menü für diese Links!)
		-> Suche	          (Inhalt: Plugin für EXT:indexed_search)
		-> TYPO3	          (externer Link: typo3.org)	
		-> CMSOD              (externer Link: cms-online-designer.de)
		
	-> Spezialseiten   (Typ: Ordner - ID wichtig für das Menü für diese Links!)
		-> Impressum	      (Inhalt: Impressum / Datenschutz)
		-> Seitenbaum	      (Inhalt: Sitemap)

	... später erstellen ...

-> Nachrichten (oder Blog)  (Ordner für EXT:news/EXT:blog - 10.04.21 noch nicht für v11)
-> FE-Benutzer              (Ordner für Frontend-User)