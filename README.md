TYPO3 Seminar Basics
====================

## Sammlung von Unterlagen zum Seminar TYPO3

Übersicht zu Snippet Sammlungen:

*   00

    Sammlung von TypoScript Snippets - vieles später mit moderneren FT Umsetzungen

*   05

    Tests mit Konstanten und Setup TypoScript; auch: Erweiterungs-Templates

*   10

    FluidTemplating mit mPurpose (Bootstrap 3) HTML Vorlage; hier noch Auslagerung in `fileadmin/templating/bu`

    Klassisches Beispiel aus Seminaren mit TYPO3 vor v9

    Hier noch keine *saubere* Ordner-Verdrahtung, wie sie später bei der Umsetzung mit Sitepackages nötig wird!

*   20

    FluidTemplating mit Bootswatch (Bootstrap 5) HTML Vorlage
    
    Hier auch noch die Implementierung in `fileadmin` aber schon saubere Ordnerstrukturen wie sie später dann auch in einer Lösung als Sitepackage/Extension genutzt werden!

    Vorschlag: Parallel-Ordner `fileadmin/templating/bu20` zu Lösung 10 nutzbar - einfach das TypoScript austauschen!

*   50

    Beispielhafte Lösung mit eigener Extension `EXT:jbspb_bootswatch`

    Gitlab Repo für die aktuellste Version: 
    
    `git clone https://gitlab.com/joebrandes/jbspb_bootswatch.git` 

    Tipp: direkt im Projekt-Ordner `./typo3conf/ext/..` unterbringen und die Extension aktivieren. Dann das Statisches Template inkludieren und ggf. das fluid_styled_content Template entfernen (dieses wird durch das Sitepackage bereits integriert). Und natürlich auch das sonstige TypoScript entfernen - jetzt liegt ja alles im Sitepackage.

...tbc...
