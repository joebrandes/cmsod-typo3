<?php
defined('TYPO3_MODE') || die();

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
   'tt_content',
   'CType',
    [
        'LLL:EXT:jbspb_bootswatch/Resources/Private/Language/locallang.xlf:jbspb_bootswatch_newcontentelement_title',
        'jbspb_bootswatch_newcontentelement',
        'content-jbspb-bootswatch-accordion',
    ],
    'textmedia',
    'after'
);


// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['jbspb_bootswatch_newcontentelement'] = [
    'showitem' => '
          --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
             --palette--;;general,
             header; Internal title (not displayed),
             bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
          --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
             --palette--;;language,
          --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
             --palette--;;hidden,
             --palette--;;access,
       ',
    'columnsOverrides' => [
       'bodytext' => [
          'config' => [
             'enableRichtext' => true,
             'richtextConfiguration' => 'default',
          ],
       ],
    ],
 ];
