######################
#### DEPENDENCIES ####
######################
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/setup.typoscript">
# <INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/Styling/setup.typoscript">

################
#### HELPER ####
################
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:jbspb_bootswatch/Configuration/TypoScript/Helper/DynamicContent.typoscript">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:jbspb_bootswatch/Configuration/TypoScript/Helper/MyHelper.typoscript">

#############################################################
#### EXTENSIONS                                          ####
#### Manual inclusion to keep control over loading order ####
#############################################################
# <INCLUDE_TYPOSCRIPT: source="FILE:EXT:jbspb_bootswatch/Configuration/TypoScript/Extension/IndexedSearch/setup.typoscript" condition="userFunc = TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('indexed_search')">
# <INCLUDE_TYPOSCRIPT: source="FILE:EXT:jbspb_bootswatch/Configuration/TypoScript/Extension/IndexedSearch/setup.typoscript" condition="extension.isLoaded('indexed_search') == 1">
# Laden mit Conditions funzt bisher nicht!
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:jbspb_bootswatch/Configuration/TypoScript/Extension/IndexedSearch/setup.typoscript">


##############
#### PAGE ####
##############
page = PAGE
page {
    typeNum = 0
    shortcutIcon = EXT:jbspb_bootswatch/Resources/Public/Icons/favicon.ico

    10 = FLUIDTEMPLATE
    10 {
        # Template names will be generated automatically by converting the applied
        # backend_layout, there is no explicit mapping necessary anymore.
        #
        # BackendLayout Key
        # subnavigation_right_2_columns -> SubnavigationRight2Columns.html
        #
        # Backend Record
        # uid: 1 -> 1.html
        #
        # Database Entry
        # value: -1 -> None.html
        # value: pagets__subnavigation_right_2_columns -> SubnavigationRight2Columns.html
        templateName = TEXT
        templateName {
            cObject = TEXT
            cObject {
                data = pagelayout
                required = 1
                case = uppercamelcase
                split {
                    token = pagets__
                    cObjNum = 1
                    1.current = 1
                }
            }
            ifEmpty = Default
        }
        templateRootPaths {
            0 = EXT:jbspb_bootswatch/Resources/Private/Templates/Page/
            1 = {$page.fluidtemplate.templateRootPath}
        }
        partialRootPaths {
            0 = EXT:jbspb_bootswatch/Resources/Private/Partials/Page/
            1 = {$page.fluidtemplate.partialRootPath}
        }
        layoutRootPaths {
            0 = EXT:jbspb_bootswatch/Resources/Private/Layouts/Page/
            1 = {$page.fluidtemplate.layoutRootPath}
        }
        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
            10 {
                references.fieldName = media
            }
            20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            20 {
                levels = 2
                includeSpacer = 1
                as = mainnavigation
            }
            30 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            30 {
                special = directory
                # special.value = 14
                special.value = {$styles.searchpageid}
                levels = 1
                includeSpacer = 1
                as = topnavigation
            }
            40 = TYPO3\CMS\Frontend\DataProcessing\LanguageMenuProcessor
            40 {
                languages = auto
                as = languageNavigation
            }
            50 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            50 {
                special = rootline
                special.range = 0|-1
                # includeNotInMenu = 1
                as = menuBreadcrumb
            }
        }
    }

    meta {
        viewport = {$page.meta.viewport}
        robots = {$page.meta.robots}
        apple-mobile-web-app-capable = {$page.meta.apple-mobile-web-app-capable}
        description = {$page.meta.description}
        description {
            override.field = description
        }
        author = {$page.meta.author}
        author {
            override.field = author
        }
        keywords = {$page.meta.keywords}
        keywords {
            override.field = keywords
        }
        X-UA-Compatible = {$page.meta.compatible}
        X-UA-Compatible {
            attribute = http-equiv
        }

        # OpenGraph Tags
        og:title {
            attribute = property
            field = title
        }
        og:site_name {
            attribute = property
            data = TSFE:tmpl|setup|sitetitle
        }
        og:description = {$page.meta.description}
        og:description {
            attribute = property
            field = description
        }
        og:image {
            attribute = property
            stdWrap.cObject = FILES
            stdWrap.cObject {
                references {
                    data = levelfield:-1, media, slide
                }
                maxItems = 1
                renderObj = COA
                renderObj {
                    10 = IMG_RESOURCE
                    10 {
                        file {
                            import.data = file:current:uid
                            treatIdAsReference = 1
                            width = 1280c
                            height = 720c
                        }
                        stdWrap {
                            typolink {
                                parameter.data = TSFE:lastImgResourceInfo|3
                                returnLast = url
                                forceAbsoluteUrl = 1
                            }
                        }
                    }
                }
            }
        }
    }

    includeCSSLibs {

    }

    includeCSS {
        # Bootswatch:
        # jbspb_bootswatch_layout = EXT:jbspb_bootswatch/Resources/Public/Css/bootswatch/cerulean/bootstrap.css
        jbspb_bootswatch_layout = EXT:jbspb_bootswatch/Resources/Public/Css/bootswatch/{$styles.bootswatch}/bootstrap.css
        jbspb_bootswatch_custom = EXT:jbspb_bootswatch/Resources/Public/Css/bootswatch/custom.min.css
        jbspb_bootswatch_custom_joeb = EXT:jbspb_bootswatch/Resources/Public/Css/bootswatch/custom.joeb.css
        # Bootswatch Icons:
        jbspb_bootswatch_icons = https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css
        jbspb_bootswatch_icons.external = 1
        # Custom Style:

        # Lightbox2
        jbspb_bootswatch_lightbox = EXT:jbspb_bootswatch/Resources/Public/JavaScript/Lightbox2/css/lightbox.css

    }

    includeJSLibs {

    }

    includeJS {
        # Klaro
        jbspb_cmsod_klaroconfig = EXT:jbspb_bootswatch/Resources/Public/JavaScript/Klaro/config.js
        jbspb_cmsod_klaro = EXT:jbspb_bootswatch/Resources/Public/JavaScript/Klaro/klaro.js
    }

    includeJSFooterlibs {
        jbspb_bootswatch_bootstrap = EXT:jbspb_bootswatch/Resources/Public/JavaScript/Dist/bootstrap.bundle.min.js
    }

    includeJSFooter {
        jbspb_bootswatch_ilovet3 = EXT:jbspb_bootswatch/Resources/Public/JavaScript/Dist/scripts.js
        # Lightbox2
        jbspb_bootswatch_lightbox = EXT:jbspb_bootswatch/Resources/Public/JavaScript/Lightbox2/js/lightbox-plus-jquery.min.js
    }
}


################
#### CONFIG ####
################
config {
    absRefPrefix = auto
    no_cache = {$config.no_cache}
    uniqueLinkVars = 1
    pageTitleFirst = 1
    linkVars = L
    prefixLocalAnchors = {$config.prefixLocalAnchors}
    renderCharset = utf-8
    metaCharset = utf-8
    doctype = html5
    removeDefaultJS = {$config.removeDefaultJS}
    inlineStyle2TempFile = 1
    admPanel = {$config.admPanel}
    debug = 0
    cache_period = 86400
    sendCacheHeaders = {$config.sendCacheHeaders}
    intTarget =
    extTarget =
    disablePrefixComment = 1
    index_enable = 1
    index_externals = 1
    index_metatags = 1
    headerComment = {$config.headerComment}

    // Disable Image Upscaling
    noScaleUp = 1

    // Compression and Concatenation of CSS and JS Files
    compressJs = 0
    compressCss = 0
    concatenateJs = 0
    concatenateCss = 0
}


# Attributes for Lightbox2 use:
lib.contentElement.settings.media.popup.linkParams.ATagParams.dataWrap = data-lightbox="gallery" data-title="{file:current:title} - {file:current:name}" alt="{file:current:alternative}" rel="lightbox[{field:uid}]"

# in Ext:tx_indexedsearch Text (de) ersetzen
# ==========================================
plugin {
  tx_indexedsearch {
    _LOCAL_LANG {
      de {
        rules.text ( 
Nur Wörter mit 2 oder mehr Zeichen werden akzeptiert.<br>
Maximal 200 Zeichen insgesamt.<br>
Leerzeichen werden zur Trennung von Worten verwendet, "" kann für die Suche nach ganzen Zeichenfolgen benutzt werden (keine Indexsuche).<br>
UND, ODER und NICHT sind Suchoperatoren, die den standardmäßigen Operator überschreiben.<br>
+/|/- entspricht UND, ODER und NICHT als Operatoren.<br>
Alle Suchwörter werden zu Kleinschreibung konvertiert.
)
      }
    }
  }
}

# Custom CType with FT
tt_content.jbspb_bootswatch_newcontentelement = < lib.contentElement
tt_content.jbspb_bootswatch_newcontentelement {
    templateName = NewContentElement
}